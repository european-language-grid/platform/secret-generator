FROM        alpine
WORKDIR     /app
LABEL       MAINTAINER="European Language Grid <coordinator@european-language-grid.eu>"
RUN         apk update
RUN         apk add wget curl git gcc
RUN         wget -nv https://git.io/goreleaser
ADD         . /app/
RUN         ["chmod", "+x", "./goreleaser"]
RUN         apk add --no-cache --virtual .build-deps bash musl-dev openssl go
RUN         go version
RUN         go mod download
RUN         ./goreleaser --debug --snapshot
RUN         cp dist/secret-generator_linux_amd64/secret-generator elg-k8s-secgen
ENTRYPOINT  ["/app/elg-k8s-secgen"]
CMD         ["-logtostderr"]